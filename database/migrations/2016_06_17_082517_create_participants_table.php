<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('phone')->unique();
            $table->string('address');

            $table->string('type');
            $table->boolean('old')->default(true);
            $table->string('price');
            $table->string('contribution');
            $table->string('period','5');
            $table->string('installment');

            $table->ipAddress('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
