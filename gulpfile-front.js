var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function(mix) {

    mix
        .sass('main.scss')
        .babel([
            'facebookUtils.js',
            'main.js'
        ], 'public/js/main.js')

        /*******************     scripts front End start here          ******************/
        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-validator/dist/validator.js',
            'bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.js',
            'bower_components/teamdf/jquery-number/jquery.number.min.js',
            // endbower
            'bower_components/es6-promise/es6-promise.js',
            '../resources/assets/js/autoNumeric.js',
            '../resources/assets/js/popper.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
        ], 'public/js/plugins-front.js', 'bower_components')
        /*******************     styles front End start here          ******************/
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css',
            'bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.css',
            // endbower
            'bower_components/font-awesome/css/font-awesome.min.css',
        ], 'public/css/plugins-front.css', 'bower_components')


});
