@servers(['web' => 'root@188.165.240.99'])

@setup
    $user = "ijara";
    $directory = "/home/ijara/public_html/ijara-2022";
    $repository = "https://digcrow@bitbucket.org/boxadvertainment/ijara-2020.git";
@endsetup

@macro('create')
    clone
    configure
@endmacro


@macro('refresh')
    delete
    clone
    configure
@endmacro

@task('clone')
    su - {{ $user }};
    git clone -b master {{ $repository }} {{ $directory }};

    cd {{ $directory }};
    {{-- composer self-update; --}}
    composer install --prefer-dist --no-dev --no-interaction;

    cp .env.production .env;

    echo "Project has been created";
@endtask

@task('pull')
    su - {{ $user }};
    cd {{ $directory }};

    git pull origin;
    composer install --prefer-dist --no-dev --no-interaction;

    cp .env.production .env;
    echo "Deployment finished successfully!";
@endtask

@task('configure')
    su - {{ $user }};
    cd {{ $directory }};

    {{-- chown -R www-data:www-data {{ $directory }}; --}}
    echo "Permissions have been set";
@endtask

@task('delete', ['confirm' => true])
    rm -rf {{ $directory }};
    echo "Project directory has been deleted";
@endtask

@task('reset', ['confirm' => true])
    cd {{ $directory }};
    git reset --hard HEAD;
@endtask

@task('migrate')
    cd {{ $directory }};
    php artisan migrate --force;
@endtask

@task('rollback')
    cd {{ $directory }};
    php artisan migrate:rollback --force;
@endtask

@task('seed')
    cd {{ $directory }};
    php artisan db:seed --force;
@endtask
