<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Vote;

class Post extends Model
{
    protected $table = 'posts';

    public function participant()
    {
        return $this->belongsTo('App\Model\Participant');
    }

    public function getVoteCount(){
        return Vote::where('participant_id', $this->participant_id )->count();
    }

    public function getNumberByStatus($status)
    {
        return Post::where('status', $status)->count();
    }

    public function getListByStatus($status)
    {
        return Post::where('status', $status)->get();
    }

    public function getCountByCategory($status)
    {
        return Post::where('axe', $status)->count();
    }



    public function getCostDetailsFileAttribute($value)
    {
        return  str_replace('/home/banque-zitouna/quiam/storage/app/participants/','', $value);
    }

    public static function getEnumColumnValues($table, $column) {

        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type ;

        preg_match('/^enum\((.*)\)$/', $type, $matches);

        $enum_values = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum_values = array_add($enum_values, $v, $v);
        }
        return $enum_values;
    }
}
