<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    public function votes(){
        return $this->hasMany('App\Model\Vote');
    }

}
