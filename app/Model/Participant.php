<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function post()
    {
        return $this->hasOne('App\Model\Post');
    }

    public function getOrganismConstitutionStatusAttribute($value)
    {
        return  str_replace('/home/banque-zitouna/quiam/storage/app/participants/','', $value);
    }

    public function getOrganismConstitutionJortAttribute($value)
    {
        return  str_replace('/home/banque-zitouna/quiam/storage/app/participants/','', $value);
    }

    public function getOrganismReferenceFileAttribute($value)
    {
        return  str_replace('/home/banque-zitouna/quiam/storage/app/participants/','', $value);
    }

}
