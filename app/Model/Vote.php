<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //
    public function post (){
        return $this->belongsTo('App\Model\Post');
    }

    public function voter (){
        return $this->belongsTo('App\Model\Voter');
    }

    public function getVoteCount($post_id){
        return Vote::where('post_id', $post_id)->count();
    }
}
