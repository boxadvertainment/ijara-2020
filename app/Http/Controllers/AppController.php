<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Model\Post;
use App\Model\Vote;
use App\Model\Voter;
use App\Model\Participant;
use Thumbnail;

class AppController extends Controller{

    public function index(){
        return view('index');
    }

    public function comingSoon(){
        return view('layout-coming-soon');
    }

    public function ijara($product){
        return view('ijara')->with([
            'product' => $product,
        ]);
    }

    public function cgu(){
        return view('cgu');
    }

    public function packPro(){
        return view('pack-pro');
    }
    public function promo(){
        return view('promo');
    }


    public function save(Request $request){


        $validator = \Validator::make($request->all(), [
            'name'                         => 'required',
            'lastname'                     => 'required',
            'phone'                        => 'required|regex:/^[0-9]{8}$/|unique:participants',
            'address'                      => 'required'
        ]);


        $validator->setAttributeNames([
            'name'                         => 'Nom',
            'lastname'                     => 'Prénom',
            'phone'                        => 'Téléphone',
            'address'                      => 'Adresse'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'code' => 102,
                'message' => implode('<br>', $validator->errors()->all())
            ]);
        }


        $participant = new Participant;

        $participant->name                    = $request->input('name');
        $participant->lastname                = $request->input('lastname');
        $participant->phone                   = $request->input('phone');
        $participant->address                 = $request->input('address');
        $participant->type                    = $request->input('type');
        $participant->old                     = $request->input('old');
        $participant->price                   = $request->input('price');
        $participant->contribution            = $request->input('contribution');
        $participant->period                  = $request->input('period');
        $participant->installment             = $request->input('installment');
        $participant->ip                      = $request->ip();


        if ( !$participant->save() ) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        return response()->json([
            'success'   => true,
            'message'   => "Merci pour votre participation."
        ]);
    }





    public function vote(Request $request){

        if(! env('VOTE_ACTIVE'))
            return response()->json(['success' => false, 'message' => 'Le vote est fermé']);

        if (Voter::where('facebook_id', $request->input('id') )->first() == null) {
            $voter = new Voter;
            $voter->facebook_id   = $request->input('id');
            $voter->name          = $request->input('name');
            $voter->email         = $request->input('id').'@facebook.com';
            $voter->ip            = $request->ip();

            if(!$voter->save()){
                return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
            }
        }else{
            $voter = Voter::where('facebook_id', $request->input('id') )->first();
        }

        if(!session('voter_id') || session('voter_id') != $voter->id ){
            session(['voter_id' => $voter->id ]);
        }

        $exist = Vote::whereRaw('voter_id = ? and  post_id = ?', [ $voter->id , $request->get('post_id') ] )->count();

        if($exist!=0){
            return response()->json(['success' => false, 'message' => 'Vous avez déja voté pour ce projet !', 'count' => Vote::where('post_id', $request->input('post_id') )->first()->getVoteCount($request->input('post_id')) ]);
        }else{
            $vote = new Vote();
            $vote->post_id = (integer) $request->get('post_id');
            $vote->voter_id = $voter->id;
            if($vote->save()){
                $post= Post::where('id', $vote->post_id )->first();
                return response()->json(['success' => true, 'message' => $post->participant->organism_name.' vous remercie pour votre Vote', 'count' => $vote->getVoteCount($vote->post_id) ]);
            }
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }
}
