<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use App\Model\Post;
use App\Model\Participant;
use Excel;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }



    public function participants(){
        return view('admin.participant')->with([
            'list' => Participant::orderBy('created_at', 'desc')->get()
        ]);
    }

    public function exportParticipants()
    {
        $participants = Participant::all();

        return Excel::create('Participants', function($excel) use ($participants) {
            $excel->sheet('Participants', function($sheet) use ($participants)
            {
                $sheet->fromModel($participants);
            });
        })->download('xls');

    }


    public function editParticipant($id, Request $request){
        $participant = Participant::find($id);
        return view('admin.editParticipant')->with([
            'participant' => $participant
        ]);
    }

    public function updateParticipant($id, Request $request){


        $validator = \Validator::make($request->all(), [
            'participant_nom'  =>  'required',
            'participant_email' =>  'required|email',
            'participant_prenom' =>  'required',
            'facebook_id'  =>  'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);
        }else{
            Participant::where('id', $id)
                ->update([
                    'participant_nom' => $request->participant_nom,
                    'participant_prenom' => $request->participant_prenom,
                    'facebook_id' => $request->facebook_id,
                    'participant_email' => $request->participant_email
                ]);
            return view('admin.participant')->with([
                'list' => Participant::all()
            ]);
        }
    }


    public function getFile($id)
    {
        $document = Document::findOrFail($id);

        $filePath = $document->file_path;

        // file not found
        if( ! Storage::exists($filePath) ) {
            abort(404);
        }

        $pdfContent = Storage::get($filePath);

        // for pdf, it will be 'application/pdf'
        $type       = Storage::mimeType($filePath);
        $fileName   = Storage::name($filePath);

        return Response::make($pdfContent, 200, [
            'Content-Type'        => $type,
            'Content-Disposition' => 'inline; filename="'.$fileName.'"'
        ]);
    }

    public function posts(){
        $p = new Post();
//        $test = Post::find(1);
//        echo $test->cost_details_file;
//        dd($test);
        return view('admin.posts')->with([
            'nbPost' => Post::all()->count(),
            'listpending' => $p->getListByStatus("pending"),
            'listaccept' => $p->getListByStatus("accepted"),
            'listrefused' => $p->getListByStatus("rejected"),
            'nbPending' => $p->getNumberByStatus("pending"),
            'nbAccepted' => $p->getNumberByStatus("accepted"),
            'nbRefused' => $p->getNumberByStatus("rejected")
        ]);
    }

    public function postDetail($id){
        $post = Post::find($id);
        return view('admin.post_detail')->with(['post' => $post ]);
    }

//    public function editCategory(){
//        $cat = Category::find(\Request::input('id'));
//        $cat->categorie_name = \Request::input('categorie_name');
//        $cat->save();
//        return redirect()->route('viewCategories');
//    }
}
