<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'AppController@comingSoon');
Route::get('/', 'AppController@index');

Route::get('/ijara/{product}' , 'AppController@ijara');
Route::get('/packs-pro/' , 'AppController@packPro');
Route::get('/promo/' , 'AppController@promo');
Route::post('/merci/' , 'AppController@save');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::auth();
    Route::get('/', 'AdminController@participants');
    Route::get('/export', 'AdminController@exportParticipants');
//    Route::get('posts/', 'AdminController@posts')->name('viewPosts');
//    Route::get('participants/', 'AdminController@participants');
//    Route::get('updatePostStatus/{status}/{id}', 'AdminController@updatePostStatus')->name('updatePost');
//
//
//    Route::get('edit-participant/{id}', 'AdminController@editParticipant');
//    Route::post('edit-participant/update/{id}', 'AdminController@updateParticipant');
});

//Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
//Route::post('signup', 'AppController@signup');

