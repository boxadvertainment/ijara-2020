<?php
namespace App\Traits;

trait EnumValue
{

    // Get enum values from given $column_name within $table_name and return array
    public function getEnumColumnValues($table_name,$column_name)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table_name WHERE Field = '{$column_name}'"))[0]->Type ;

        preg_match('/^enum\((.*)\)$/', $type, $matches);

        $enum_values = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum_values = array_add($enum_values, $v, $v);
        }

        return $enum_values;
    }

}
