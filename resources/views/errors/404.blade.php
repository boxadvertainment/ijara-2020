@extends('errors.layout')

@section('main_container')
        <!-- page content -->
        <div id="notfound">
            <div class="notfound">
                <div class="notfound-404">
                    <h1>4<span></span>4</h1>
                </div>
                <h2>OUPS! PAGE PAS TROUVÉE</h2>
                <p>Désolé mais la page que vous recherchez n'existe pas, a été supprimée, le nom a changé ou elle est temporairement indisponible</p>
                <a href="/">Retour à la page d'accueil</a>
            </div>
        </div>

        <!-- /page content -->

@endsection

