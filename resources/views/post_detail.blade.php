@extends('layout')

@section('main_container')

    <div class="banner-innerpage mb-5"></div>

    <div class="container">
        <div class="project">
            <div class="row">
                <div class="col-12">
                    <h3 class="project_title"><small class="text-uppercase text">Details du projet de : </small>
                        <br>{{ $post->participant->organism_name }}</h3>
                </div>
                <div class="col-md-6">
                    <p class="">Projet d'une <b>{{$post->participant->organism_category }}</b> qui touche : <b class="text-secondary">{{$post->axe }}</b></p>
                    <p class="project_desc">{{$post->description }}</p>

                    @if( env('VOTE_ACTIVE') ||  app('request')->input('heyyyy') == '1')
                        @if($voteCount > 1)
                        <p><b class="text-secondary votes-count">{{ $voteCount }}</b> personnes ont déjà voté pour ce projet.</p>
                        @elseif($voteCount == 1)
                        <p><b class="text-secondary votes-count">{{ $voteCount }}</b> personne a déjà voté pour ce projet.</p>
                        @else
                            <p>Aucune personne n'a voté pour ce projet. Soyez le premier</p>
                        @endif
                        <p class="text-muted"><small>Soutenez le projet de "{{ $post->participant->organism_name }}" et partagez le, pour augmenter leur chances pour gagner. Merci !</small></p>
                    @endif

                    <div class="btn-wrapper">
                        @if( env('VOTE_ACTIVE') ||  app('request')->input('heyyyy') == '1')
                            @if( !$votedFor )
                                <button class="btn btn-secondary btn-vote" data-id="{{ $post->id }}"> <i class="fa fa-heart"></i> Likez </button>
                            @else
                                Vous avez déjà voté :
                            @endif
                        @endif
                            <button class="btn fb-share" data-share=""> <i class="fa fa-share-alt"></i> Partagez </button>
                            <a href="/projets" class="btn"> <i class="fa fa-arrow-circle-left"></i> Voir les autres projets </a>
                    </div>
                </div>
                <div class="col-md-6 video-wrapper">
                    <video
                            id="vid1"
                            src="{{asset('storage/participants/'.$post->youtube_video_id) }}"
                            class="video-js vjs-default-skin vjs-big-play-centered"
                            controls
                            width="100%" height="280px"
                    >
                    </video>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('meta.title', 'J\'ai votez pour '. $post->participant->organism_name .', et vous ? - Kiyam par Banque Zitouna')
@section('meta.description', ' قيم, مبادرة من مصرف الزيتونة لدعم المشاريع الخيرية المقترحة من الجمعيات و المؤسسات الاجتماعية والعمومية' )
