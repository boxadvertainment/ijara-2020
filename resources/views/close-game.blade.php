@extends('layout')

@push('stylesheets')

@section('main_container')

    <div class="container">
        <div class="cta-text btn-wrapper">
            <div class="col-md-12">
                <img src="{{ asset('images/be-creative.png') }}" alt="Be Creative By BH" class="becreative-logo">
            </div>
            <div class="col-md-12 text-center">
                <h1 class="title">Les votes sont clos !</h1>
                <p>Les 2 gagnants seront annoncés sur la page FB de la BH :</p> <br>
                <a href="https://www.facebook.com/BHTunisie/" class="btn share-fb"><i class="fa fa-facebook"></i> /BHTunisie </a>
                <br> <br>
            </div>
            <div class="col-md-8 col-md-offset-2 video-wrapper">
                <iframe width="100%" height="290px" src="https://www.youtube.com/embed/U_nFO9ukFmU" frameborder="0" allowfullscreen=""></iframe>
            </div>

        </div>
    </div>

    {{--<div id="addPostModal" class="modal animated fadeInDown"  tabindex="-1" role="dialog" aria-labelledby="addPostModal" aria-hidden="true" data-backdrop="static">--}}
        {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-body form">--}}
                    {{--<div class="profile-block text-center">--}}
                        {{--<div class="profile-picture"><img src="{{ asset('images/profile-pic-placeholder.png') }}" alt=""></div>--}}
                    {{--</div>--}}
                    {{--<div class="form-info-bloc text-center">--}}
                        {{--<h3 class="title">Merci de remplir le formulaire</h3>--}}
                        {{--<p>Pour participer, il faut etre en poste dans une agence de communication et avoir un passeport valide--}}
                            {{--<br><small>Nb : Le candidat pré-sélectionné devra déposer sa demande de visa, si elle n'est pas acceptée un autre candidat sera désigné par le jury pour le remplacer.</small></p>--}}
                    {{--</div>--}}
                    {{--<form class="clearfix form-participent" method="post" action="{{ url('insertion') }}" enctype="multipart/form-data" id="addPost" data-toggle="validator">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<input type="hidden" name="facebook_id" class="user-fb-id">--}}
                        {{--<div class="col-sm-12">--}}

                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Votre nom & prénom * :</span>--}}
                                        {{--<input type="text" name="name" required="required" class="form-control user-name" placeholder="Votre nom & prénom..." title="Ce champ est obligatoire">--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Votre date de naissance < 30 * :</span>--}}
                                        {{--<input type="text" data-theme="bh" data-lang="fr" data-min-year="1987" data-default-date="07-01-1987" data-max-year="2005" data-large-mode="true" data-format="m-Y" data-translate-mode="true" readonly id="birthday" name="birth_date" class="date-picker form-control col-sm-5" placeholder="Votre date de naissance..." title="Ce champ est obligatoire">--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Votre adresse email * :</span>--}}
                                        {{--<input type="email" name="email" required="required" class="form-control user-email" placeholder="Votre adresse email..." title="Email incorrect">--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Votre numéro de téléphone * :</span>--}}
                                        {{--<input type="tel" name="phone" required="required" class="form-control" placeholder="Votre numéro de téléphone..." maxlength="8" required pattern="^[25493][0-9]{7}$" title="Téléphone incorrect">--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Agence * :</span>--}}
                                        {{--<input type="text" name="agency" required="required" class="form-control" placeholder="Votre agence..." title="Ce champ est obligatoire">--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Poste occupé * :</span>--}}
                                        {{--<input type="text" name="post_title" required="required" class="form-control" placeholder="Le poste occupé, copyrighter..." title="Ce champ est obligatoire">--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Vous avez un passeport valide ? *</span><br>--}}
                                        {{--<label class="switch">--}}
                                            {{--<input type="checkbox" name="has_passport" required="required" title="Ce champ est obligatoire" value="1">--}}
                                            {{--<div class="slider round"></div>--}}
                                        {{--</label>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">--}}
                                        {{--<span>Vous avez un visa Schengen valide ?</span> <br>--}}
                                        {{--<label class="switch">--}}
                                            {{--<input type="checkbox" name="has_visa" value="1">--}}
                                            {{--<div class="slider round"></div>--}}
                                        {{--</label>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group post-media col-sm-12">--}}
                                {{--<label class="control-label">--}}
                                    {{--<span>Votre vidéo * :</span>--}}
                                    {{--<div class="dropzone-wrapper" id="preview">--}}
                                        {{--<div class="file"><i class="fa fa-file"></i></div>--}}
                                        {{--<div class="file-name">Cliquer ou placer votre vidéo ici</div>--}}
                                        {{--<div class="file-size">  Taille max : 80 Mo  </div>--}}
                                        {{--<input type="file" name="video" required="required" class="form-control dropzone-input">--}}

                                    {{--</div>--}}

                                {{--</label>--}}
                            {{--</div>--}}

                            {{--<div class="form-group col-sm-12">--}}
                                {{--<p class="info"><small>* Champ obligatoire</small></p>--}}
                            {{--</div>--}}

                            {{--<div class="form-group modal-btns clearfix">--}}
                                {{--<button type="submit" class="btn btn-success">Valider</button>--}}
                                {{--<button type="reset" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Annuler</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection