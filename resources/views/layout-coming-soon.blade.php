<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title'){{ Config::get('app.name') }}</title>

    @include('partials.socialMetaTags', [
        'title' => 'Coming Soon',
        'description' => ""
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    <link rel="shortcut icon" type="image/png" href="/favicon.ico">

    <link rel="stylesheet" href="{{ asset('css/plugins-front.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.7.2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        FB_APP_ID   = '{{ env('FACEBOOK_APP_ID') }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset("js/modernizr.js") }}"></script>
</head>
<body class="@yield('class')" style="background: none">



    <!-- App Wrapper -->
    <div id="wrapper" class="main_container">

        <img src="{{ asset('images/coming-soon.jpg') }}" alt="" width="100%">

    </div>
    <!-- /#wrapper -->

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-lg-left links mb-3 text-center text-md-left">
                    <a href="https://www.facebook.com/BanqueZitouna/" target="_blank" class="rs-icon"><img src="{{asset('images/fb.png')}}" alt=""></a>
                    <a href="https://lu.linkedin.com/company/zitouna-bank" target="_blank" class="rs-icon"><img src="{{asset('images/in.png')}}" alt=""></a>
                    <a href="https://instagram.com/banquezitouna" target="_blank" class="rs-icon"><img src="{{asset('images/insta.png')}}" alt=""></a>
                    <br>
                    <span class="copy">© 2020 Banque Zitouna</span>
{{--                    <a href="/reglement.pdf" target="_blank">Réglement </a> | <a href="/cgu">Conditions générales</a>--}}
                </div>
                <div class="col-md-4 text-center text-dark mb-3">
                </div>
                <div class="col-md-4 text-center text-lg-right mb-3">
                    <img src="{{asset('images/bz-logo-footer.png')}}" alt="">
                </div>
            </div>
        </div>
    </footer>

    @if(app()->environment() == 'production')
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160294755-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-160294755-1');
        </script>
    @endif

    <!-- Scripts -->
    <script src="{{ asset("js/plugins-front.js") }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ asset('js/autoNumeric.js') }}"></script>
    <script src="{{ asset("js/main.js") }}"></script>


    @stack('scripts')

</body>
</html>
