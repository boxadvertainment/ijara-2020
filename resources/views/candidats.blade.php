@extends('layout')

@push('stylesheets')

@section('main_container')

    <div class="banner-innerpage mb-5"></div>


    <div class="container list-steps">

        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="text-center mb-5">
                    <h1 class="mb-3">Comment participer ?</h1>
                    <p class="lead">Vous êtes un représentant légal d’un organisme tunisien et vous œuvrez  pour promouvoir la qualité de l’éducation, soutenir la santé publique ou lutter contre la pauvreté ?</p>
                </div>
                <div class="row justify-content-center ">
                    <div class="col-lg-9">
                        <h5 class="text-center mb-3">Suivez ces étapes pour déposer votre projet :</h5>
                        <ul class="list-unstyled">
                            <li><img src="{{asset('images/list-1.png')}}" alt=""> Lire <a href="/cgu">les conditions générales</a> de soumission </li>
                            <li><img src="{{asset('images/list-2.png')}}" alt=""> Remplir <a href="/inscription">le formulaire</a> au complet</li>
                            <li><img src="{{asset('images/list-3.png')}}" alt=""> Soumettre le projet</li>
                        </ul>

                        <br>

                        <h5 class="text-center mb-3">Aprés le dépot de candidature :</h5>
                        <ul class="list-unstyled">
                            <li><img src="{{asset('images/list-1.png')}}" alt=""> Votre demande sera traitée </li>
                            <li><img src="{{asset('images/list-2.png')}}" alt=""> Vous recevrez un e-mail de l'avancement du traitement.</li>
                        </ul>

                        <br>

                        <h5 class="text-center mb-3">Aprés la validation du projet:</h5>
                        <ul class="list-unstyled">
                            <li><img src="{{asset('images/list-1.png')}}" alt=""> Votre projet sera affiché dans la catégorie " Consultez les projets"</li>
                            <li><img src="{{asset('images/list-2.png')}}" alt=""> Le vote sera possible à partir de la date du 24/02/2020</li>
                        </ul>
                    </div>
                </div>



            </div>
        </div>

    </div>
@endsection
