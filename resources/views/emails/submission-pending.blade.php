<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>{{ Config::get('app.name') }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="color:#363636; margin: 0; padding: 0; background:#dcdedd; font-family: Arial, sans-serif; font-size: 14px;">
<center>
    <p style="padding: 0 0 25px; font-size: 0px;"></p>
<table width="600px" cellpadding="0" border="0" style="background:#FFF; margin-bottom:30px;">
    <tr>
        <td>
            <center><img src="{{ asset('images/kiyam-logo.png') }}" alt="Kiyam" /></center>
        </td>
    </tr>
    <tr><td>
            <table cellpadding="45">
                <tbody>
                <tr>
                    <td>
                        <table border="0"  cellspacing="0">

                            <tr>
                                <td>
                                    <h2 style="margin-bottom:10px; font-size: 20px;">Bonjour {{ $name }},</h2>
                                    <p style="font-size:18px;">Votre projet vient d’être soumis sur la plateforme Kiyam. Notre équipe va d’abord en prendre connaissance avant publication.</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" style="color:#80848b; font-size:14px;">
                                        <tr>
                                            <td width="120" valign="middle"><center><img src="{{ asset('images/list-1.png') }}" alt="Revue" /></center></td>
                                            <td valign="top"><p><b>Notre équipe passe en revue les informations de votre projet</b> afin de s’assurer qu’il est conforme aux conditions générales d’utilisation</p></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle"><center><img src="{{ asset('images/list-2.png') }}" alt="Analyse" /></center></td>
                                            <td valign="top"><p>Après l’analyse par notre équipe, <b>vous recevez un email pour vous avertir de la publication de votre projet ou de sa non-conformité</b></p></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle"><center><img src="{{ asset('images/list-3.png') }}" alt="Annonce" /></center></td>
                                            <td valign="top"><p>Une fois votre projet publié, vous pourrez <b>le relayer auprès de votre entourage pour inciter au vote</b>. N’oubliez pas, plus il y'a de partages, plus le nombre de votes augmente !</p></td>
                                        </tr>
                                    </table>
                                    <p style="font-size: 18px;"><b>A bientôt sur la plateforme Kiyam !</b></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

     </td></tr>
 </table>
    <table>
        <tr>
            <td>
                <center>

                    <a href="https://www.facebook.com/BanqueZitouna/" target="_blank" class="rs-icon"><img src="{{asset('images/fb.png')}}" alt="Facebook" width="30px"></a>
                    <a href="https://lu.linkedin.com/company/zitouna-bank" target="_blank" class="rs-icon"><img src="{{asset('images/in.png')}}" alt="LinkedIn" width="30px"></a>
                    <a href="https://instagram.com/banquezitouna" target="_blank" class="rs-icon"><img src="{{asset('images/insta.png')}}" alt="Instagram" width="30px"></a>
                    <br>
                    <br>
                    <span class="copy">© 2020 Banque Zitouna</span> <br> <br>
                    <img src="{{asset('images/bz-logo-footer-2.png')}}" width="180px" alt="Banque Zitouna">
                </center>
            </td>
        </tr>
    </table>
</center>
</body>
</html>
