@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">

        <div class="col-sm-12">
            <h1>Liste : <a href="admin/export" class="btn btn-success pull-right"> Exporter </a></h1>
        </div>
        <div class="row">
            @foreach($list as $key => $participant)
                <div class="col-md-4">
                    <div class="thumbnail card-post">
                        <div class="card-body">
                            <h3 class="text-primary">{{$participant->name }}  {{$participant->lastname }}</h3>
                            <p>Phone : <i class="fa fa-phone"></i>{{$participant->phone }} - Adresse  : {{$participant->address }}  </p>
                            <h4>Ijara {{$participant->type }}</h4>
                            <ul class="list-unstyled user_data">
                                <li>Type de Bien : {{ ($participant->old == 1) ? 'Neuf' : 'Occasion' }} </li>
                                <li>Montant du Bien en TTC : {{ $participant->price }}</li>
                                <li>Premier Loyer : {{ $participant->contribution }}</li>
                                <li>Durée de Remboursement : {{ $participant->period }} an(s)</li>
                                <li class="text-primary"> Mensualité : <b> {{ number_format((float) $participant->installment, 3) }} </b></li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
    <!-- /page content -->

@endsection

@push('scripts')

@endpush
