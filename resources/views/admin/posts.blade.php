@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <div class="right_col" role="main">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i></div>
                    <div class="count">{{ $nbPost }}</div>
                    <h3>Posts</h3>
                    <p>Total numbers of posts.</p>
                </div>
            </div>
        </div>


        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Pending <span class="badge">{{ $nbPending }}</span></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Accepted <span class="badge">{{ $nbAccepted }}</span></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Refused <span class="badge">{{ $nbRefused }}</span></a>
                </li>
            </ul>


            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                    @if($listpending!=null)
                        <div class="row postblock">
                            @foreach($listpending as $key => $post)
                                <div class="thumbnail card-post">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="text-primary">{{$post->participant->organism_name }} <span class="label label-success"> {{$post->axe }}</span> <span class="label label-warning"> {{$post->participant->organism_category }}</span></h3>
                                            <p>Representant : <b>{{$post->participant->organism_representative }} </b> - {{$post->participant->organism_representative_position }}  <a href="mailto:{{$post->participant->organism_representative_email }}"><i class="fa fa-envelope"></i> {{$post->participant->organism_representative_email }}</a> <i class="fa fa-phone"></i> {{$post->participant->organism_representative_phone }} </p>
                                        </div>

                                        <div class="col-md-3">
                                            <br>
                                            <a href="{{ url('admin/updatePostStatus/accepted/'.$post->id) }}" class="btn btn-success btn-product"><span class="fa fa-check"></span> Accept</a>
                                            <a href="{{ url('admin/updatePostStatus/rejected/'.$post->id) }}" class="btn btn-danger btn-product"><span class="fa fa-trash"></span> Refuse</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <video width="100%" height="230" controls>
                                            <source src="{{asset('storage/participants/'.$post->youtube_video_id) }}" type="video/mp4">
                                        </video>
                                    </div>
                                    <div class="col-md-5">
                                        <h4>Description du Projet</h4>

                                        <p>{{$post->description }}</p>

                                        <h4>Details Projet</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-map-marker"></i> {{$post->region }} </li>
                                            <li> <i class="fa fa-clock-o"></i> {{$post->progress_period }} jours</li>
                                            <li> <i class="fa fa-users"></i> <b>{{$post->beneficiary }}</b> | <b class="text-primary"> Nombre :  {{$post->beneficiary_number }}</b></li>
                                            <li><a href="{{asset('storage/participants/'.$post->cost_details_file) }}" target="_blank"> <i class="fa fa-file-pdf-o"></i> Détails des dépenses </a> </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">

                                        <h4>Details Organisme</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-phone"></i> {{$post->participant->organism_phone }} </li>
                                            <li><a href="mailto:{{$post->participant->organism_email }}"><i class="fa fa-envelope"></i> {{$post->participant->organism_email }}</a></li>
                                        </ul>

                                        <h4>Les fichier</h4>
                                        <ul class="list-unstyled project_files">
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_reference_file) }}" target="_blank"><i class="fa fa-file-picture-o"></i> Les références</a>
                                            </li>
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_constitution_status) }}" target="_blank"><i class="fa fa-file-pdf-o"></i> Status</a>
                                            </li>
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_constitution_jort) }}" target="_blank"><i class="fa fa-file-pdf-o"></i> Jort</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h3 class="text-center">Aucune post!!!</h3>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane" id="tab_content2" aria-labelledby="profile-tab">
                    @if($listaccept!=null)
                        <div class="row postblock">
                            @foreach($listaccept as $key => $post)
                                <div class="thumbnail card-post">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="text-primary">{{$post->participant->organism_name }} <span class="label label-success"> {{$post->axe }}</span> <span class="label label-warning"> {{$post->participant->organism_category }}</span></h3>
                                            <p>Representant : <b>{{$post->participant->organism_representative }} </b> - {{$post->participant->organism_representative_position }}  <a href="mailto:{{$post->participant->organism_representative_email }}"><i class="fa fa-envelope"></i> {{$post->participant->organism_representative_email }}</a> <i class="fa fa-phone"></i> {{$post->participant->organism_representative_phone }} </p>
                                        </div>

                                        <div class="col-md-3">
                                            <br>
                                            <a href="{{ url('admin/updatePostStatus/rejected/'.$post->id) }}" class="btn btn-danger btn-product"><span class="fa fa-trash"></span> Refuse</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <video width="100%" height="230" controls>
                                            <source src="{{asset('storage/participants/'.$post->youtube_video_id) }}" type="video/mp4">
                                        </video>
                                    </div>
                                    <div class="col-md-5">
                                        <h4>Description du Projet</h4>

                                        <p>{{$post->description }}</p>

                                        <h4>Details Projet</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-map-marker"></i> {{$post->region }} </li>
                                            <li> <i class="fa fa-clock-o"></i> {{$post->progress_period }} jours</li>
                                            <li> <i class="fa fa-users"></i> <b>{{$post->beneficiary }}</b> | <b class="text-primary"> Nombre :  {{$post->beneficiary_number }}</b></li>
                                            <li><a href="{{asset('storage/participants/'.$post->cost_details_file) }}" target="_blank"> <i class="fa fa-file-pdf-o"></i> Détails des dépenses </a> </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">

                                        <h4>Details Organisme</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-phone"></i> {{$post->participant->organism_phone }} </li>
                                            <li><a href="mailto:{{$post->participant->organism_email }}"><i class="fa fa-envelope"></i> {{$post->participant->organism_email }}</a></li>
                                        </ul>

                                        <h4>Les fichier</h4>
                                        <ul class="list-unstyled project_files">
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_reference_file) }}" target="_blank"><i class="fa fa-file-picture-o"></i> Les références</a>
                                            </li>
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_constitution_status) }}" target="_blank"><i class="fa fa-file-pdf-o"></i> Status</a>
                                            </li>
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_constitution_jort) }}" target="_blank"><i class="fa fa-file-pdf-o"></i> Jort</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h3 class="text-center">Aucune post!!!</h3>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane" id="tab_content3" aria-labelledby="profile-tab">
                    @if($listrefused!=null)
                        <div class="row postblock">
                            @foreach($listrefused as $key => $post)
                                <div class="thumbnail card-post">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="text-primary">{{$post->participant->organism_name }} <span class="label label-success"> {{$post->axe }}</span> <span class="label label-warning"> {{$post->participant->organism_category }}</span></h3>
                                            <p>Representant : <b>{{$post->participant->organism_representative }} </b> - {{$post->participant->organism_representative_position }}  <a href="mailto:{{$post->participant->organism_representative_email }}"><i class="fa fa-envelope"></i> {{$post->participant->organism_representative_email }}</a> <i class="fa fa-phone"></i> {{$post->participant->organism_representative_phone }} </p>
                                        </div>

                                        <div class="col-md-3">
                                            <br>
                                            <a href="{{ url('admin/updatePostStatus/accepted/'.$post->id) }}" class="btn btn-success btn-product"><span class="fa fa-check"></span> Accept</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <video width="100%" height="230" controls>
                                            <source src="{{asset('storage/participants/'.$post->youtube_video_id) }}" type="video/mp4">
                                        </video>
                                    </div>
                                    <div class="col-md-5">
                                        <h4>Description du Projet</h4>

                                        <p>{{$post->description }}</p>

                                        <h4>Details Projet</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-map-marker"></i> {{$post->region }} </li>
                                            <li> <i class="fa fa-clock-o"></i> {{$post->progress_period }} jours</li>
                                            <li> <i class="fa fa-users"></i> <b>{{$post->beneficiary }}</b> | <b class="text-primary"> Nombre :  {{$post->beneficiary_number }}</b></li>
                                            <li><a href="{{asset('storage/participants/'.$post->cost_details_file) }}" target="_blank"> <i class="fa fa-file-pdf-o"></i> Détails des dépenses </a> </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">

                                        <h4>Details Organisme</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-phone"></i> {{$post->participant->organism_phone }} </li>
                                            <li><a href="mailto:{{$post->participant->organism_email }}"><i class="fa fa-envelope"></i> {{$post->participant->organism_email }}</a></li>
                                        </ul>

                                        <h4>Les fichier</h4>
                                        <ul class="list-unstyled project_files">
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_reference_file) }}" target="_blank"><i class="fa fa-file-picture-o"></i> Les références</a>
                                            </li>
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_constitution_status) }}" target="_blank"><i class="fa fa-file-pdf-o"></i> Status</a>
                                            </li>
                                            <li><a href="{{asset('storage/participants/'.$post->participant->organism_constitution_jort) }}" target="_blank"><i class="fa fa-file-pdf-o"></i> Jort</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h3 class="text-center">Aucune post!!!</h3>
                    @endif
                </div>
            </div>
        </div>

    </div>

@endsection
