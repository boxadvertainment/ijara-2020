@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

        <div class="right_col" role="main">
            <div class="">
                <div class="col-sm-12">
                    <h1>Liste categories</h1>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="col-md-2 col-md-offset-10">
                                    <a href="{{ url('admin/form-category') }}" class="btn btn-primary">Add</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <!-- start project list -->
                                <table class="table table-striped projects">
                                    <thead>
                                    <tr>
                                        <th style="width: 1%">Id</th>
                                        <th style="width: 20%">Category Name</th>
                                        <th>Project Progress</th>
                                        <th>Post number</th>
                                        <th>Created at</th>
                                        <th style="width: 20%">#Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($list as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->categorie_name }}</td>
                                        <td class="project_progress">
                                            <div class="progress progress_sm">
                                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ $item->getPourcentage() }}"></div>
                                            </div>
                                            <small>{{ $item->getPourcentage() }} %</small>
                                        </td>
                                        <td>{{ $item->getNumber() }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>
                                            <a href="{{ url('admin/postByCategories/'.$item->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> List </a>
                                            <a href="{{ url('admin/category-detail/'.$item->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- end project list -->

                            </div>
                        </div>
                    </div>

            </div>
        </div>

@endsection

