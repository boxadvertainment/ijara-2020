<meta name="description" content="@yield('description', $description)">

<div class="thumbnail" >
    <div class="row">
        <div class="col-md-12">
            <h3>Org <small> org category</small> — <smal> axe </smal></h3>
            <small><b>Redp </b> - fonction — <a href="mailto:">email</a> | 55467500 </small>
        </div>
        <div class="col-md-4">
            <video width="100%" height="230" controls>
                <source src="@yield('youtube_video_id')" type="video/mp4">
            </video>
        </div>
        <div class="col-md-8">
            <h4>Description du Projet</h4>

            <p>description</p>

            <h4>Details Projet</h4>
            <ul class="list-unstyled user_data">
                <li><i class="fa fa-map-marker"></i> Region </li>
                <li> <i class="fa fa-clock-o"></i> Progress</li>
                <li> <i class="fa fa-users"></i> Benef - <span class="font-weight-bold">20112</span></li>
                <li> <i class="fa fa-users"></i>  </li>
                <li> <i class="fa fa-users"></i> Benef </li>
                <li> <i class="fa fa-users"></i> Benef </li>
                <li><a href="" target="_blank"> <i class="fa fa-file-pdf-o"></i> Details depenses</a> </li>
            </ul>

            <h4>Details Organisme</h4>
            <ul class="list-unstyled user_data">
                <li><i class="fa fa-phone"></i> 12121212 </li>
                <li><a href="mailto:"><i class="fa fa-envelope"></i> email</a></li>
            </ul>

            <h4>Les fichier</h4>
            <ul class="list-unstyled project_files">
                <li><a href="" target="_blank"><i class="fa fa-file-picture-o"></i> Les références</a>
                </li>
                <li><a href="" target="_blank"><i class="fa fa-file-pdf-o"></i> Status</a>
                </li>
                <li><a href="" target="_blank"><i class="fa fa-file-pdf-o"></i> Jort</a>
                </li>
            </ul>

        </div>
    </div>
</div>

    <div class="caption">
        <div class="row post-detail">
            <h4>@yield('youtube_video_id')</h4>
            <ul>
                <li>Agnece : <b>{{ $item->participant->agency }}</b> | Post : <b>{{ $item->participant->post_title }}</b></li>
                <li>Birthday : <b>{{ $item->participant->birth_date }}</b></li>
                <li>Email : <b>{{ $item->participant->email }}</b> || Phone: <b>{{ $item->participant->phone }}</b></li>
                <li>Passport : <b>{{ $item->participant->has_passport }}</b> || Visa: <b>{{ $item->participant->has_visa }}</b></li>
            </ul>
        </div>
        <div class="row text-center">
            <a href="{{ url('admin/updatePostStatus/accept/'.$item->id) }}" class="btn btn-success btn-product"><span class="glyphicon glyphicon-ok-sign"></span> Accept</a>
            <a href="{{ url('admin/updatePostStatus/refused/'.$item->id) }}" class="btn btn-danger btn-product"><span class="glyphicon glyphicon-remove-sign"></span> Refuse</a>
        </div>
    </div>
</div>
