@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <div class="right_col" role="main">
        <div class="">
            <div class="col-sm-12">
                <h1>List post category : <strang>{{ $category }}</strang></h1>
            </div>

            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <a href="javascript:history.back();" class="btn btn-primary">Retour</a>
                        <div class="clearfix"></div>
                    </div>
                    @if(!$list->isEmpty())
                        <div class="x_content">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Participant Name</th>
                                    <th>Text</th>
                                    <th>Date</th>
                                    <th>Region</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($list as $item)
                                    <tr>
                                        <th scope="row">{{ $item->participant->getFullName() }}</th>
                                        <td>{{ substr($item->post_text,0,55) }}...</td>
                                        <td>{{ $item->post_date }}</td>
                                        <td>{{ $item->post_region }}</td>
                                        <td>
                                            @if($item->post_status == "pending")<span class="badge bg-orange">{{ $item->post_status }} </span>@endif
                                            @if($item->post_status == "accept")<span class="badge bg-green">{{ $item->post_status }} </span>@endif
                                            @if($item->post_status == "refused")<span class="badge bg-red">{{ $item->post_status }} </span>@endif
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/detail/'.$item->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    @else
                        <h3 class="text-center">Aucune post!!!</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

