@extends('layout')

@push('stylesheets')

@section('main_container')

    <div class="banner-innerpage mb-5"></div>


    <div class="container list-steps">

        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="text-center mb-5">
                    <h1 class="mb-3">Conditions générales de soumission</h1>
                </div>
                <div class="row justify-content-center ">
                    <div class="col-lg-6">
                        <h5 class="mb-3">Comment répondre à l’appel à projets? </h5>
                        <ul class="list-unstyled list-special">
                            <li> Remplir le formulaire en ligne, accessible sur la plateforme <span class="arabic-text">قيام</span></li>
                            <li> Télécharger les documents et supports demandés :
                                <ul>
                                    <li>Budget prévisionnel du projet à subventionner</li>
                                    <li>Documents juridiques de l’organisme porteur du projet</li>
                                    <li>Vidéo de présentation du projet</li>
                                    <li>Documents de présentation de l’organisme et des références et projets antérieurs</li>
                                </ul>
                            </li>
                            <li> Cocher les rubriques d’attestation de bonne gouvernance</li>
                            <li> Soumettre la demande de subvention</li>
                        </ul>

                        <br>


                        <h5 class="mb-3">Quelles sont les critères d’éligibilité?</h5>
                        <ul class="list-unstyled list-special">
                            <li> Vous êtes un représentant légal d’un organisme tunisien </li>
                            <li> Télécharger les documents et supports demandés :
                                <ul>
                                    <li>Association à but non lucratif</li>
                                    <li>Structure publique à intérêt général</li>
                                    <li>Entreprise sociale appuyant l’économie sociale et solidaire</li>
                                </ul>
                            </li>
                            <li> Vous œuvrez pour :
                                <ul>
                                    <li>Promouvoir la qualité d’éducation en Tunisie</li>
                                    <li>Soutenir la santé publique</li>
                                    <li>Lutter contre la pauvreté et la disparité régionale</li>
                                </ul>
                            </li>
                            <li> Votre projet est réalisation dans un délai maximum de 2 mois (8 semaine)</li>
                            <li>Si vous représentez une association, cette dernière doit être juridiquement constituée selon le décret-loi n°2011-88, du 24 septembre 2011. L’association doit répondre à un objet d'intérêt général et conforme aux valeurs de Banque Zitouna (Banque Islamique)
                            </li>
                            <li>Si vous représentez une entreprise privée, cette dernière doit être juridiquement constituée. L’activité de l’entreprise doit s’inscrire dans le cadre de l’économie sociale et solidaire et conforme aux valeurs de Banque Zitouna (Banque Islamique)</li>
                            <li>Si vous êtes une structure publique, un accord préalable de soumission est recommandé auprès de la tutelle
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <h5 class="mb-3">Quelles sont les exigences d’admissibilité des projets ?</h5>
                        <ul class="list-unstyled list-special">
                            <li> Projets éligibles : les projets présentés doivent être à l’initiative de l’organisme soumissionnaire qui en assure également la mise en œuvre. Les projets présentés doivent avoir un impact sur la région et auprès de la population : ils doivent présenter une utilité sociale durable. </li>
                            <li> Projets non éligibles :
                                <ul>
                                    <li>Les projets visant le seul bénéfice de l’organisme soumissionnaire</li>
                                    <li>Les projets qui ne s’inscrivent pas dans une dynamique sociale à l’échelle locale ou régionale;</li>
                                    <li>Les demandes se limitant à une action ponctuelle limitée dans le temps et à impact réduit ;</li>
                                    <li>Les actions de formation.</li>
                                </ul>
                            </li>
                        </ul>

                        <br>

                        <h5 class="mb-3">Quelles sont les conditions de financement ?</h5>
                        <ul class="list-unstyled list-special">
                            <li> Le seuil maximal de subvention par projet est fixé à 20.000TND
                                <br>
                                La subvention à attribuer couvre les coûts de mise en œuvre du projet en question. Ces coûts peuvent comprendre :

                                <ul>
                                    <li>Coûts du matériel et équipements</li>
                                    <li>Coût de main d’œuvre</li>
                                    <li>Frais de gestion à concurrence de 5% maximum de la subvention</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>



            </div>
        </div>

    </div>
@endsection
