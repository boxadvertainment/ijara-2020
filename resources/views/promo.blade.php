@extends('layout')

@push('stylesheets')
@section('class','simulator')

@section('main_container')

    <div class="banner-innerpage text-center mb-5">
        <h1>les NOUVEAUTÉs</h1>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row sim-title">
                    <div class="icn-wrap smaller d-block d-md-inline-block">
                        <img src="{{ asset('images/icn-pro.png') }}" class="icn icn-mehnia"/>
                    </div>
                    <div class="col">
                        <h2 class="ijara-title">
                            <small class="text-dark">NOUVEAUTÉ</small>
                            <br>
                            <span>Offre Promo</span>
                        </h2>
                    </div>
                    <div class="col-12 text-center">
                        <img src="{{asset('images/title-sep.png')}}" alt="">
                    </div>
                </div>

            </div>
            <div class="col-md-10">
                <p class="lead">
                    Profitez de l’offre Promo pour nos financements Ijara, destinée aux professionnels
                </p>
                <ul style="font-size: 1.25rem" class="my-3 ">
                    <li>Autofinancement : 0%  </li>
                    <li>Délai de réponse : 0 Jour d’attente (notification d’accord)</li>
                    <li>Frais d’étude de dossier : 0 TND (Franco) *
                        <div style="font-size: 1rem" class="text-muted mt-2">
                            <p class="mb-1">* Cet avantage est valable pour les clients équipés par l'un des trois Packs Pro, destinés aux professionnels.</p>
                            <ul class="mt-0">
                                <li>Business First </li>
                                <li>Business Optimum</li>
                                <li>Business Premium</li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <hr class="mt-3 float-left" width="80px">
                <br>
                <br>
                <p class="text-muted" style="font-size: 13px;line-height: 16px;">
                    Cette offre est valable du 05 Octobre 2020 jusqu'au 05 Janvier 2021. <br>
                    Cette offre est valable sous réserve d’acceptation de dossier.
                </p>
            </div>
        </div>
    </div>
@endsection
