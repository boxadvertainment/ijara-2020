@extends('layout')

@push('stylesheets')
@section('class','simulator')

@section('main_container')

    <div class="banner-innerpage text-center mb-5">
        <h1>Simulateur Ijara</h1>
    </div>

    <script>
        var product = ['mehnia', 'ennakel', 'tebbia', 'akkarat'].indexOf('{{ $product }}');
    </script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="row sim-title">
                    <div class="icn-wrap smaller d-block d-md-inline-block">
                        <img src="{{ asset('images/icn-'.$product.'.png') }}" class="icn icn-mehnia"/>
                    </div>
                    <div class="col">
                        <h2 class="ijara-title">
                            <small class="text-dark">IJARA</small>
                            <br>
                            <span></span>
                        </h2>
                    </div>
                    <div class="col-12 text-center">
                        <img src="{{asset('images/title-sep.png')}}" alt="">
                    </div>
                </div>

            </div>
            <div class="col-md-7">
                <form action="#" class="step-1 needs-validation" data-toggle="validator" >
                    <div class="row">


                        <div class="col-md-12">
                            <div class="form-group">
                                <label><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Type de Bien :</label> <br>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="old" value="1.19" id="radionew" onchange="handleInputChange(this)" class="custom-control-input" required>
                                    <label class="custom-control-label" for="radionew">Neuf</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="old" value="1" onchange="handleInputChange(this)" id="radioold" class="custom-control-input" required>
                                    <label class="custom-control-label" for="radioold">Occasion</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="product_price"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Montant du Bien en TTC :</label>
                                <div class="input-group">

                                    <input type="text" name="product_price" autoComplete="false" class="form-control money"
                                           id="product_price" placeholder="Montant" required onkeyup="handleInputChange(this)" disabled />
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">DT TTC</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="contribution"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Premier Loyer :</label>
                                <div class="input-group">

                                    <input type="text" name="contribution" autoComplete="false" class="form-control money"
                                           id="contribution" placeholder="Apport" onkeyup="handleInputChange(this)" disabled="" />
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">DT TTC</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="period"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Durée de Remboursement :</label>
                                <div>
                                    <input name="period" class="form-control custom-range hide" id="period"
                                           type="text" required onchange="handleInputChange(this)" disabled />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <form class="form-sim needs-validation" data-toggle="validator" method="post" action="/merci" id="simulator" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" name="old">
                    <input type="hidden" name="price">
                    <input type="hidden" name="contribution">
                    <input type="hidden" name="period">
                    <input type="hidden" name="installment">
                    <input type="hidden" name="type">

                    <div class="step-2 hide">
                        <p class="text-center">Veuillez saisir vos coordonnées, afin <br>
                            qu’un chargé de clientèle puisse vous contacter </p>
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Votre nom :</label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Votre nom" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Votre prénom :</label>
                                    <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Votre prénom" required />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Votre téléphone :</label>
                                    <input type="tel" name="phone" class="form-control" id="phone" placeholder="Votre téléphone" maxlength="8" required />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address"><img src="{{ asset('images/list-input.png') }}" alt="" width="25"> Votre adresse :</label>
                                    <input type="text" name="address" class="form-control" id="address" placeholder="Votre adresse" required />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button class="btn btn-primary" type="submit">Validez</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer-ijara">
        <div class="container">
            <div class="step-1">
                <div class="inst-box installement"><small>Mensualité :</small>  <span>0,000 <sup>DT TTC</sup></span> </div>
                <h5 class="text-center periodPlace pt-4 text-white">Sur <span>0 an</span></h5>
                <hr class="bg-white step-1">
                <div class="text-center p-5">
                    <a href="#" class="btn btn-contact bg-white text-primary btn-lg text-uppercase">Contactez- moi</a>
                </div>
            </div>
            <div class="step-2 p-5 hide"></div>


        </div>
    </footer>
@endsection

@push('scripts')
    <script>

        var mpa = ['0.0746','0.0733','0.0739','0.0749','0.0761','0.0774','0.0787','0.0801',	'0.0814',	'0.0827'];
        var packs = [
            {
                name : 'Mouaddet Mehnia',
                min: 1,
                max: 7
            },
            {
                name : 'Mouaddet Ennakel',
                min: 1,
                max: 5
            },
            {
                name : 'Mouaddet Tebbia',
                min: 1,
                max: 7
            },

            {
                name : 'Akkarat',
                min: 2,
                max: 10
            }
        ];



        let instalment,
            price = 0,
            old = 1,
            contribution = 0,
            period = 5;


        var slideLabel = null,slideTicks;


        let selectedPack = packs[ product ];

        $('.ijara-title span').html(selectedPack.name);

        if(slideLabel != null)
            $("#period").slider('destroy');

        if(selectedPack.max == 5){
            slideTicks = [1, 2, 3, 4, 5];
            slideLabel = ["1 an", "2 ans", "3 ans", "4 ans", "5 ans"];
        }else if(selectedPack.max == 7){
            slideTicks = [1, 2, 3, 4, 5, 6, 7];
            slideLabel = ["1 an", "2 ans", "3 ans", "4 ans", "5 ans", "6 ans", "7 ans"];
        }else{
            slideTicks = [2, 3, 4, 5, 6, 7, 8,9,10];
            slideLabel = ["2 ans", "3 ans", "4 ans", "5 ans", "6 ans", "7 ans", "8 ans","9 ans","10 ans"];
        }
        $("#period").slider({
            min: selectedPack.min,
            max: selectedPack.max,
            ticks: slideTicks,
            ticks_labels: slideLabel,
            ticks_tooltip: true,
            step: 1,
            value: selectedPack.max,
        });

        function handleInputChange(event){

            if(event.name === 'old'){
                old = event.value;
                $('input[disabled]').removeAttr('disabled');
            }

            if(event.name === 'product_price'){
                price = parseInt(event.value.replace(/ /g, ''));
            }


            if(event.name === 'contribution'){
                contribution =  parseInt(event.value.replace(/ /g, ''));
                if(contribution >= price){
                    contribution = 0 ;
                    event.value = 0;
                }

            }

            if(event.name === 'period'){
                period = event.value;

            }

            let base =  (price  /  old ) - (contribution/1.19) ;
            let mg = Number(mpa[period - 1]) * Number(period);
            let pv = Number(base * ( 1 + mg ));

            instalment = pv / (Number(period) * 12) ;

            console.log(old,price,contribution,period);
            let unit;

            if( old == 1.19){
                unit = ' <sup>DT HT</sup>';
            }else{
                unit = ' <sup>DT TTC</sup>';
            }
            if(period > 1){
                $('.periodPlace span').html( period + ' ans' );
            }else{
                $('.periodPlace span').html( period + ' an' );
            }
            $('.installement span').html( $.number( instalment, 3, ',', ' ' ) + unit );
        }
    </script>
@endpush
