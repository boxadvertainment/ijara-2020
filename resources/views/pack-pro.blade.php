@extends('layout')

@push('stylesheets')
@section('class','simulator')

@section('main_container')

    <div class="banner-innerpage text-center mb-5">
        <h1>les NOUVEAUTÉS</h1>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="row sim-title">
                    <div class="icn-wrap smaller d-block d-md-inline-block">
                        <img src="{{ asset('images/icn-pro.png') }}" class="icn icn-mehnia"/>
                    </div>
                    <div class="col">
                        <h2 class="ijara-title">
                            <small class="text-dark">NOUVEAUTÉ</small>
                            <br>
                            <span>Packs Pro</span>
                        </h2>
                    </div>
                    <div class="col-12 text-center">
                        <img src="{{asset('images/title-sep.png')}}" alt="">
                    </div>
                </div>

            </div>
            <div class="col-md-7">
                <p class="lead">
                    Profitez de la nouvelle offre packagée destinée aux professionnels. <br>

                    Banque Zitouna met à votre disposition 3 Packs Pro. <br>

                    Bénéficiez de services qui répondent à vos besoins tout en profitant :
                </p>
                <ul style="font-size: 1.25rem;">
                    <li>D’une tarification profitable ;</li>
                    <li>De conditions préférentielles sur les opérations courantes ;</li>
                    <li>D’une rémunération de vos dépôts ;</li>
                    <li>De couvertures assurance et assistances avantageuses ;</li>
                </ul>
                <p class="text-center">
                    <img src="{{ asset('images/tableau.png') }}" class="img-fluid" alt="">
                </p>
            </div>
        </div>
    </div>
@endsection
