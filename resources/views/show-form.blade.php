@extends('layout')

@push('stylesheets')
@section('class','inscription')

@section('main_container')

    <div class="banner-innerpage mb-5"></div>

    <div class="container inner-page">
        <div class="hero text-center mb-4">
            <h2>Déposer votre projet</h2>
            <p>Pour participer et gagner veuillez remplir tout les champs du fomulaire de façon sincère et complète</p>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-9">
                <form class="form-participent needs-validation" novalidate method="post" action="{{ url('insertion') }}" enctype="multipart/form-data" id="addPost" role="form" data-toggle="validator">
                    {{ csrf_field() }}
                    <div id="quiamForm">
                        <ul>
                            <li><a href="#step1">1<br /><small>Renseignements sur le projet à subventionner</small></a></li>
                            <li><a href="#step2">2<br /><small>Renseignements sur l'organisme et le responsable du projet</small></a></li>
                            <li><a href="#step3">3<br /><small>Validation de vos données</small></a></li>
                        </ul>

                        <div>

                            <div id="step1" class="">
                                <div class="row form-step-wrapper" id="formStep0" role="form" data-toggle="validator">
                                    <div class="col-12">
                                        <h4>Renseignements sur le projet à subventionner</h4>
                                        <p class="info"><small>Tous les champs sont obligatoires</small></p>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="formOrgCat1">Vous êtes :</label>
                                                    <select class="custom-select" name="posts_category" id="formOrgCat1" required>
                                                        <option value="" disabled selected>Catégorie de l'organisme</option>
                                                        <option value="Entreprise publique">Entreprise publique</option>
                                                        <option value="Entreprise privée">Entreprise privée</option>
                                                        <option value="Association">Association</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formProjectObjective">Axe du projet :</label>
                                                    <select class="custom-select" name="axe" id="formProjectObjective" required>
                                                        <option value="" disabled selected>Domaine d'intervention</option>
                                                        <option value="Santé">Santé</option>
                                                        <option value="Education">Education</option>
                                                        <option value="Lutte contre la pauvreté et la disparité régionale">Lutte contre la pauvreté et la disparité régionale</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formProjectType">Type de projet :</label>
                                                    <select class="custom-select" name="type" id="formProjectType" required>
                                                        <option value="" disabled selected>Type de projet à subventionner</option>
                                                        <option value="Projet en création">Projet en création</option>
                                                        <option value="Projet en développement">Projet en développement</option>
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="formProjectDesc">Déscriptif général de votre projet : </label>
                                                    <textarea name="description" id="formProjectDesc" class="form-control" required maxlength="1300" minlength="20" rows="4"></textarea>
                                                    <small class="form-text text-muted">Décrivez brievement votre projet. Ne pas dépasser les 200 mots</small>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formRegion">Région d'intervention :</label>
                                                    <select class="custom-select" name="region" id="formRegion" required>
                                                        <option value="" disabled selected>Selectionnez une region</option>
                                                        <option value="Gouvernorat de l'Ariana">Gouvernorat de l'Ariana</option>
                                                        <option value="Gouvernorat de Béja">Gouvernorat de Béja</option>
                                                        <option value="Gouvernorat de Ben Arous">Gouvernorat de Ben Arous</option>
                                                        <option value="Gouvernorat de Bizerte">Gouvernorat de Bizerte</option>
                                                        <option value="Gouvernorat de Gabès">Gouvernorat de Gabès</option>
                                                        <option value="Gouvernorat de Gafsa">Gouvernorat de Gafsa</option>
                                                        <option value="Gouvernorat de Jendouba">Gouvernorat de Jendouba</option>
                                                        <option value="Gouvernorat de Kairouan">Gouvernorat de Kairouan</option>
                                                        <option value="Gouvernorat de Kasserine">Gouvernorat de Kasserine</option>
                                                        <option value="Gouvernorat de Kébili">Gouvernorat de Kébili</option>
                                                        <option value="Gouvernorat du Kef">Gouvernorat du Kef</option>
                                                        <option value="Gouvernorat de Mahdia">Gouvernorat de Mahdia</option>
                                                        <option value="Gouvernorat de Manouba">Gouvernorat de Manouba</option>
                                                        <option value="Gouvernorat de Médenine">Gouvernorat de Médenine</option>
                                                        <option value="Gouvernorat de Monastir">Gouvernorat de Monastir</option>
                                                        <option value="Gouvernorat de Nabeul">Gouvernorat de Nabeul</option>
                                                        <option value="Gouvernorat de Sfax">Gouvernorat de Sfax</option>
                                                        <option value="Gouvernorat de Sidi Bouzid">Gouvernorat de Sidi Bouzid</option>
                                                        <option value="Gouvernorat de Siliana">Gouvernorat de Siliana</option>
                                                        <option value="Gouvernorat de Sousse">Gouvernorat de Sousse</option>
                                                        <option value="Gouvernorat de Tataouine">Gouvernorat de Tataouine</option>
                                                        <option value="Gouvernorat de Tozeur">Gouvernorat de Tozeur</option>
                                                        <option value="Gouvernorat de Tunis">Gouvernorat de Tunis</option>
                                                        <option value="Gouvernorat de Zaghouan">Gouvernorat de Zaghouan</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formProjectDuree">Période de déroulement du projet : <output class="output" data-unit="jour">10 jours</output></label>
                                                    <input type="range" name="progress_period" value="10" min="1" max="60" class="custom-range" id="formProjectDuree" required>
                                                    <small class="form-text text-muted">Période de déroulement du projet qui ne depasse pas les 60 jours (8 semaines)</small>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formProjectBenef">Population bénéficiant du projet</label>
                                                    <input type="text" name="beneficiary" autocomplete="false" class="form-control" id="formProjectBenef" placeholder="Population bénéficiant du projet" required>
                                                    <small class="form-text text-muted">Groupe cible : ce projet vise-t-il les éleves, les patients, les citoyens, les habitants d'une ville,...</small>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formProjectBenefNumb">Nombre de personnes bénéficiaires :</label>
                                                    <input type="number" name="beneficiary_number" min="1" max="1000000" value="1" autocomplete="false" class="form-control" id="formProjectBenefNumb" required>
                                                    <small class="form-text text-muted">Nombre approximatif des personnes bénéficiaires du projet</small>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="formCostDetails">Détails des dépenses :</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="cost_details_file" class="custom-file-input" id="formCostDetails" lang="fr" required accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/vnd.ms-excel" data-filesize="30000" data-filesize-error="Max 30Mb">
                                                        <label class="custom-file-label" for="formCostDetails"></label>
                                                    </div>
                                                    <small class="form-text text-muted">Charger un fichier PDF, Word ou Excel</small>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="formProjectVideo">Votre vidéo :</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="youtube_video_id" class="custom-file-input" id="formProjectVideo" lang="fr" required accept="video/mp4,video/x-flv,video/x-msvideo,video/avi,video/mpeg,video/quicktime,video/MP2T,application/x-mpegURL,video/3gpp" data-filesize="80000" data-filesize-error="Max 80Mb">
                                                        <label class="custom-file-label" for="formProjectVideo">Cliquer ou placer votre vidéo ici</label>
                                                    </div>
                                                    <small class="form-text text-muted">Vidéo de taille max. : 80 Mo et qui ne doit pas dépasser 1<sup>min</sup>30<sup>sec</sup></small>
                                                </div>
                                            </div>

                                            <div class="col-12 mt-5 text-center">
                                                <button class="btn btn-success" type="button" data-next="2">Suivant <i class="fa fa-arrow-circle-right"></i></button>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="step2" class="">
                                <div class="row form-step-wrapper" id="formStep1" role="form" data-toggle="validator">
                                    <div class="col-12">
                                        <h4>Renseignements sur l'organisme</h4>
                                        <p class="info"><small>Tous les champs sont obligatoires</small></p>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="formOrgName">Organisme porteur du projet :</label>
                                                    <input type="text" name="organism_name" autocomplete="false" class="form-control" id="formOrgName" placeholder="Nom de l'organisme" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formOrgCat">Catégorie de l'organisme :</label>
                                                    <select class="custom-select" name="organism_category" id="formOrgCat" required>
                                                        <option value="" disabled selected>Catégorie de l'organisme</option>
                                                        <option value="Entreprise publique">Entreprise publique</option>
                                                        <option value="Entreprise privée">Entreprise privée</option>
                                                        <option value="Association">Association</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="formOrgType">Type de l'organisme : </label>
                                                    <select class="custom-select" name="organism_type" id="formOrgType" required>
                                                        <option value="" disabled selected>Type de l'organisme</option>
                                                        <option value="Etablissement scolaire">Etablissement scolaire</option>
                                                        <option value="Hopital publique">Hopital publique</option>
                                                        <option value="Startup à caractère social">Startup à caractère social</option>
                                                        <option value="Association à but non lucratif">Association à but non lucratif</option>
                                                        <option value="Club scolaire ou universitaire">Club scolaire ou universitaire</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formOrgtel">Téléphone de contact :</label>
                                                    <input type="text" name="organism_phone" autocomplete="false" class="form-control" id="formOrgtel" placeholder="Numéro de téléphone" required pattern="^[254937][0-9]{7}$" maxlength="8">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formOrgemail">Email de contact :</label>
                                                    <input type="email" name="organism_email" class="form-control" id="formOrgemail" placeholder="Adresse email" required pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$">
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <hr class="my-4">
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="lead mb-0">Documents juridiques de l'organisme</label>
                                                    <small class="form-text text-muted">Publication JORT et statut pour les associations et les entreprises privées.(PDF, Word)</small>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="formOrgStat">Statut :</label>
                                                            <div class="custom-file">
                                                                <input type="file" name="organism_constitution_status" class="custom-file-input" id="formOrgStat" lang="fr" required  accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/vnd.ms-excel" data-filesize="30000" data-filesize-error="Max 30Mb">
                                                                <label class="custom-file-label" for="formCostDetails">Statut </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="formOrgJort">Jort :</label>
                                                            <div class="custom-file">
                                                                <input type="file" name="organism_constitution_jort" class="custom-file-input" id="formOrgJort" lang="fr" required accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/vnd.ms-excel" data-filesize="30000" data-filesize-error="Max 30Mb">
                                                                <label class="custom-file-label" for="formCostDetails">Jort</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <hr class="my-4">
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="formOrgRef">Références et projets antérieurs :</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="organism_reference_file" class="custom-file-input" id="formOrgRef" lang="fr" required accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/vnd.ms-excel" data-filesize="30000" data-filesize-error="Max 30Mb">
                                                        <label class="custom-file-label" for="formOrgRef">Vos références</label>
                                                    </div>
                                                    <small class="form-text text-muted">Charger un fichier PDF, Word ou Excel</small>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Liens réseaux sociaux : <small class="text-info">Ce n'est pas obligatoire</small></label>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text"><i class="fa fa-globe"></i></div>
                                                                    </div>
                                                                    <input type="url" class="form-control" name="social_link_web" placeholder="Site web">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text"><i class="fa fa-facebook"></i></div>
                                                                    </div>
                                                                    <input type="url" class="form-control" name="social_link_fb" placeholder="Page Facebook">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text"><i class="fa fa-instagram"></i></div>
                                                                    </div>
                                                                    <input type="url" class="form-control" name="social_link_insta" placeholder="Compte Instagram">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text"><i class="fa fa-youtube"></i></div>
                                                                    </div>
                                                                    <input type="url" class="form-control" name="social_link_yt" placeholder="Compte Youtube">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text"><i class="fa fa-twitter"></i></div>
                                                                    </div>
                                                                    <input type="url" class="form-control" name="social_link_tw" placeholder="Compte Twitter">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text"><i class="fa fa-linkedin"></i></div>
                                                                    </div>
                                                                    <input type="url" class="form-control" name="social_link_in" placeholder="Compte Linkedin">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                    <div class="col-12 mt-4">
                                        <h4>Renseignements sur le responsable du projet</h4>
                                        <p class="info"><small>Tous les champs sont obligatoires</small></p>
                                    </div>

                                    <div class="col-12">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formRepName">Responsable du projet :</label>
                                                    <input type="text" name="organism_representative" autocomplete="false" class="form-control" id="formRepName" placeholder="Nom et Prénom" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formRepeFunc">Fonction :</label>
                                                    <input type="text" name="organism_representative_position" autocomplete="false" class="form-control" id="formRepeFunc" placeholder="Fonction auprès de l'organisme" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formReptel">Téléphone :</label>
                                                    <input type="text" name="organism_representative_phone" autocomplete="false" class="form-control" id="formReptel" placeholder="Numéro de téléphone" required pattern="^[254937][0-9]{7}$" maxlength="8">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="formRepemail">Email :</label>
                                                    <input type="email" name="organism_representative_email" class="form-control" id="formRepemail" placeholder="Adresse email" required pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$">
                                                </div>
                                            </div>

                                            <div class="col-md-12 mt-3">
                                                <div class="form-group">
                                                    <label for="" class="mb-0">Par l’introduction de ma demande, j’atteste :</label>
                                                    <small class="form-text text-muted mt-0">Merci de cocher les trois rubriques pour soumettre votre appel à projets</small>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="check1" id="customCheck1" required value="true">
                                                    <label class="custom-control-label" for="customCheck1"><small>Que le présent formulaire et ses annexes sont remplis de façon sincère et complète</small></label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="check2" id="customCheck2" required value="true">
                                                    <label class="custom-control-label" for="customCheck2"><small>Que la subvention demandée dans le présent formulaire ne fera pas l’objet d’un autre subventionnement d’aucune forme</small></label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="check3" id="customCheck3" required value="true">
                                                    <label class="custom-control-label" for="customCheck3"><small>Avoir pris connaissance et m’engager à respecter le règlement de cet appel à projets.</small></label>
                                                </div>
                                            </div>

                                            <div class="col-12 mt-5 text-center">
                                                <button class="btn btn-dark mr-2" type="button" data-prev="1"><i class="fa fa-arrow-circle-left"></i> Précédent </button>
                                                <button class="btn btn-success ml-2" type="submit"> Valider et envoyer <i class="fa fa-arrow-circle-right"></i></button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="step3" class="">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-center">
                                            <img src="{{ asset('images/end.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </form>
            </div>
        </div>


    </div>

@endsection
