@extends('layout')

@push('stylesheets')



@section('main_container')
        <section id="banner" class="banner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-lg-5 text-left p-0 d-none d-md-block"><img src="{{asset('images/banner-img.png')}}" alt=""></div>
                    <div class="col-md-8 col-lg-7 text-right d-flex flex-column" dir="rtl">
                        <h1 class="big">
                            <span class="big">ننصحوك ،</span>
                            <br>
                            وبأحسن
                            تمويلات
                            <br>
                            اجارة نودّوك
                        </h1>
                        <div class="buttons">
                            <a href="#ijara" smooth-scroll class="btn-img"> <img src="{{ asset('images/btn-sim.png') }}" class="img-fluid" alt=""></a>
                            <a href="#temoignages" smooth-scroll class="btn-img"> <img src="{{ asset('images/btn-tem.png') }}" class="img-fluid" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ijara section-padding" id="ijara">
            <div class="container">
                <div class="section-title text-center">
                    <h2>Simulez votre financement</h2>
                    <img src="{{asset('images/title-sep.png')}}" alt="">
                    <p>Selon votre besoin, sélectionnez le produit <br>
                        qui vous convient</p>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="/ijara/mehnia" class="ijara-product box-link">
                            <div class="icn-wrap">
                                <img src="{{ asset('images/icn-mehnia.png') }}" class="icn icn-mehnia"/>
                            </div>
                            <b>Ijara</b>
                            Mouaddet Mehnia
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="/ijara/ennakel" class="ijara-product box-link">
                            <div class="icn-wrap">
                                <img src="{{ asset('images/icn-ennakel.png') }}" class="icn icn-ennakel"/>
                            </div>
                            <b>Ijara</b>
                            Mouaddet Ennakel
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="/ijara/tebbia" class="ijara-product box-link">
                            <div class="icn-wrap">
                                <img src="{{ asset('images/icn-tebbia.png') }}" class="icn icn-tebbia"/>
                            </div>
                            <b>Ijara</b>
                            Mouaddet Tebbia
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="/ijara/akkarat" class="ijara-product box-link">
                            <div class="icn-wrap">
                                <img src="{{ asset('images/icn-akkarat.png') }}" class="icn icn-akkarat"/>
                            </div>
                            <b>Ijara</b>
                            Akkarat
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="products section-padding" id="products">
            <div class="container">
                <div class="section-title text-center">
                    <h2>Nouveautés</h2>
                    <img src="{{asset('images/title-sep.png')}}" alt="">
                    <p>Découvrez les nouveautés dédiées aux <br>
                        professionnels</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="/packs-pro" class="ijara-product box-link">
                            <div class="icn-wrap">
                                <img src="{{ asset('images/icn-pro.png') }}" class="icn icn-mehnia"/>
                            </div>
                            Packs Pro
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="/promo" class="ijara-product box-link">
                            <div class="icn-wrap">
                                <img src="{{ asset('images/icn-offre.png') }}" class="icn icn-ennakel"/>
                            </div>
                            Offre Promo
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="temoignages section-padding" id="temoignages">
            <div class="container">
                <div class="section-title text-center">
                    <h2>Regardez les témoignes <br>
                        de nos clients</h2>
                    <img src="{{asset('images/title-sep.png')}}" alt="">
                </div>
                <div class="row pt-4">
                    <div class="col-md-6 col-lg-3 px-5 py-3">
                        <a href="#" class="testi-box" data-toggle="modal" data-src="Xh8ZtrmA-rE" data-target="#videoModal">

                            <img src="{{ asset('images/vid-2.png') }}" class="img-fluid mb-4" alt="">
                            <div class="txt">
                                <h5><img src="{{ asset('images/testi-list.png') }}" alt=""> M. Issam Barhoumi</h5>
                                <p>Propriètaire de Samurai <br>
                                    gym club</p>
                            </div>


                        </a>
                    </div><div class="col-md-6 col-lg-3 px-5 py-3">
                        <a href="#" class="testi-box" data-toggle="modal" data-src="VoOVbfuMN4c" data-target="#videoModal">

                            <img src="{{ asset('images/vid-3.png') }}" class="img-fluid mb-4" alt="">
                            <div class="txt">
                                <h5><img src="{{ asset('images/testi-list.png') }}" alt=""> M. Anouar Ayed</h5>
                                <p>Propriètaire de Bodytec <br>
                                    Studio </p>
                            </div>


                        </a>
                    </div><div class="col-md-6 col-lg-3 px-5 py-3">
                        <a href="#" class="testi-box" data-toggle="modal" data-src="UWWFrNBxOWE" data-target="#videoModal">

                            <img src="{{ asset('images/vid-4.png') }}" class="img-fluid mb-4" alt="">
                            <div class="txt">
                                <h5><img src="{{ asset('images/testi-list.png') }}" alt=""> M. Abderrahmen Bettaher</h5>
                                <p>Producteur d'huile d'olive</p>
                            </div>


                        </a>
                    </div><div class="col-md-6 col-lg-3 px-5 py-3">
                        <a href="#" class="testi-box" data-toggle="modal" data-src="tEZDOb6nYUY" data-target="#videoModal">

                            <img src="{{ asset('images/vid-1.png') }}" class="img-fluid mb-4" alt="">
                            <div class="txt">
                                <h5><img src="{{ asset('images/testi-list.png') }}" alt=""> Dr  Ridha el Gafsi</h5>
                                <p>Médecin Radiologue</p>
                            </div>


                        </a>
                    </div>
                </div>
            </div>
        </section>


        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9" id="video">
                            <div class="loader show"><div class="spinner"> <small>   Veuillez Patienter </small>  <div></div> <div></div> <div></div> </div></div></div>
                    </div>

                </div>
            </div>
        </div>
@endsection
